#Folgende Bibliotheken werden in diesem Script verwendet:
#"urllib" --> dient zum Lesen und Öffnen der URLs
#"json" --> dient dazu json-Dateien einlesen und in Python einfach auslesbar machen
#"zipfile" --> hilft bei der Erstellung und Verwaltung der zip-Ordner
#"os" -->
#"csv" --> dient zur Erstellung und zum Auslesen von csv-files
#"datetime" --> dient hier um die Daten der Zeitungartikel schön darzustellen

import urllib.request
import json
import zipfile
import os
import csv
import datetime

#einlesen der URL des IIIF-Manifests mit den Metadaten und Links der zu herunterladenden Zeitungsartikel
url = 'https://ub-iiifpresentation.ub.unibas.ch/zas_flex_manifest/0837d639fb06b97a0ab31b1542994e95/flex_manifest/'
#die URL wird geöffnet
iiif_manifest = urllib.request.urlopen(url)
#die IIIF-Daten werden gelesen
iiif_data = iiif_manifest.read()
#der Inhalt der IIIF-Daten wird in UTF-8 codiert abgeholt
data_utf8 = iiif_manifest.info().get_content_charset('utf-8')
#mit Hilfe der JSON-Bibliothek werden die IIIF-Daten als JSON lesbar gemacht
json= json.loads(iiif_data.decode(data_utf8))

#der zip-Ordner, in dem der ganze Download gespeichert wird, wird im Schreibmodus initiert
zip_ordner = zipfile.ZipFile('download_zeitungsartikel.zip', mode='w') 
#Ein leerer Ordner "pdf" wird in die zip-Datei gelegt, der später mit den pdf-Dateien gefüllt werden kann.
zip_ordner.write('pdf')


"""Die Quelldaten der Zeitungsartikel werden in der IIIF-Datei im Format yyy-mm-dd dargestellt.
Gewünscht ist aber das besser lesbare Format dd.mm.yyy.
Diese einfache Funktion ändert die Formate um. """

def datumformatieren(datum):
    return datetime.datetime.strptime(datum, "%Y-%m-%d").strftime("%d.%m.%Y")

""" Die Metadaten werden als Liste zurückgeliefert.
Dies sieht bei der Ausgabe aufgrund der Klammern und Anführungszeichen unschön aus.
Diese Funktion macht aus den Listen schön formatierte Strings. """

def liste_zu_string(liste): 
    #Ein leerer String wird initiiert.
    neuer_string = "" 
    """Es gibt Metadatenfelder, die mehrere Einträge enthalten können.
    Dass heisst, bei der Ausgabe hat die Liste mehr als ein Treffer.
    Diese Einträge sollen zusätzlich zur Formatierung auch per Komma getrennt werden.
    Mit einer if-Bedingung werden die Listen, die mehr als ein Eintrag haben herausgefiltert. """
    if len(liste) > 1:
        """"Bei Listen mit mehreren Einträge sollen diese per Komma getrennt werden und 
        dem String hinzugefügt werden, aber nicht beim letzten Element der Liste.
        Daher gibt es zwei verschiedene for-Schleifen, die mit Slicing zuerst den
        letzten und zweitletzten Einträge auslesen und mit einem Komma ergänzen.
        Die zweite for-Schleife dient für den letzten Eintrag, hier wird kein Komma eingefügt."""
        for metadata in liste[0:-1]:
            neuer_string += metadata + ", "
        for metadata in liste[-1]:
            neuer_string += metadata
   #Auch Listen mit einem Eintrag werden in einen String umgewandelt.       
    else:
        for metadata in liste:
            neuer_string += metadata
    #Die Funktion gibt den schön formatierten String zurück.
    return neuer_string

#Die Funktion "download_file" dient dem Download der pdf-Dateien.

def download_file(pdf_url_einzeln, dateiname):
    """Bei einigen PDFs gibt es URLs die Umlaute und/oder Leerschläge enthalten.
    Umlaute uns Leerschläge können nicht richtitg interpretiert und ausgegeben werden
    und müssen daher in ASCII-Codierung umgewandelt werden.
    Dies wird hier mit dem Befehl "parse" von urllib gemacht.
    Nur werden auch die Doppelpunkte in der URL durch diesen Befehl mit '%3A' ersetzt.
    Dies wird mit "replace" wieder rückgängig gemacht. """
    url_encodiert = urllib.parse.quote(pdf_url_einzeln)
    schoene_url = urllib.request.urlopen(url_encodiert.replace('%3A', ':'))   
    #Es wird eine pdf-Datei mit dem extrahierten Titel aus der IIIF-Datei erstellt.
    pdf_datei = open(dateiname, 'wb')
    #In die leere Datei wird der Inhalt der Datei geschrieben, die mit der URL geöffnet wird.
    pdf_datei.write(schoene_url.read())
    #Die PDF-Datei wird wieder geschlossen.
    pdf_datei.close()
    """"Einige wenige Dateinamen haben einen Apostroph im Titel.
    Dies führt beim Öffnen in der HTML-Datei zu Fehlern.
    Daher wird hier beim Speichern der Datei im zip-Ordner auch der Apostroph entfernt."""
    zip_ordner.write(dateiname, 'pdf\\'+dateiname.replace("'", ""))
    #Die erstellten Dateien werden entfernt, da diese bereits ins zip-Verzeichnis kopiert wurden.
    os.remove(dateiname)


""""Hier wird das csv-file für die Metadaten initiert. 
Mit newline wird jeder Eintrag auf eine neue Zeile geschrieben.
Bei writerow können die gewünschten Spaltenüberschriften eingefügt werden."""
csv_file = open("metadaten.csv", "w", encoding="UTF-8", newline='')
csv_objekt = csv.writer(csv_file)
csv_objekt.writerow(["Titel", "Zeitungsname", "Quelldatum", "Ausgabe", "Seite", "Autor*innen", 
                     "Thematisches Schlagwort", "Geografisches Schlagwort", "Personenschlagwort", 
                     "Firmenschlagwort", "Link"])

    
#Hier wird die HTML-Indexdatei für die Navigation eröffnet.        
html_file = open('index.html','w', encoding="utf-8")
"""Mit dem Befehl "write" wird der gröste Teil der HTML-Datei geschrieben.
Die Inhalte der HTML-Datei wurden von der Webseite der UB Basel übernommen.
Daher werden nur die Teile kommentiert, die für dieses Script hinzugefügt wurden."""

html_file.write("""<html lang="en">
<head>
<meta charset="utf-8">
<!-- Hier wird der Titel der Index-Seite eingefügt. -->
<title>Download Zeitungsartikel</title>
<link rel="icon" type="image/x-icon" href="style/favicon.ico">
<link rel="stylesheet" type="text/css" href="style/style_sheet.css" media="all">
</head>

<body class="no-repeat"><p></p>
<div class="d-block header-unibas">
<div class="bg-unibas-mint brand">
    <div class="d-flex h-100 container standard">
        <div class="flex-fill logo-left d-flex flex-column">
            <a href="https://www.ub.unibas.ch" class="d-flex flex-column flex-grow-1 align-items-start">
                    <img alt="Universität Basel" src="style/uni_logo.svg">
            </a>
                    <p><a href="https://www.ub.unibas.ch">Universitätsbibliothek</a></p>
        </div>
                <a class="d-none d-md-block" href="https://wirtschaftsarchiv.ub.unibas.ch/">
                        <img class="logo-right" alt="Universitätsbibliothek" src="style/swa_logo.svg">
                </a>
    </div>
</div>
<div class="bg-unibas-mint-light brand light">
    <div class="container standard">
        <p class="m-0"><a href="https://wirtschaftsarchiv.ub.unibas.ch/de/">Schweizerisches Wirtschaftsarchiv SWA</a></p>
    </div>
</div>
<div class="container unibas content-unibas bg-white">
<div id="c768" class="unibas-element frame frame-default frame-type-textmedia frame-layout-0">
<div class="ce-textpic ce-right ce-intext"><div class="ce-bodytext">

<!-- Hier wurde der Text mit den rechtlichen Vorgaben eingefügt. -->
<p><p><b>Nutzungsbedingungen: Urheberrecht und Haftungsausschluss </b><p>
Die auf der Plattform «Rechercheportal Zeitungsauschnitte» zugänglichen Dokumente sind ausschnittsweise 
Reproduktionen von schweizerischen Tages-und Wochenzeitungen. Die Ausschnitte im Dateiformat PDF stehen für 
nichtkommerzielle Zwecke in Lehre und Forschung sowie für den privaten Eigengebrauch für registrierte 
Nutzerinnen und Nutzer frei zur Verfügung.
<br>
Die Einhaltung der urheberrechtlichen Bestimmungen liegt in der Verantwortung der Benutzenden. 
Das Schweizerische Wirtschaftsarchiv und die Universitätsbibliothek Basel übernehmen keine Haftung für die 
widerrechtliche Verwendung digitaler Dokumente und für Schäden, die durch die Verwendung bzw. das 
Fehlen von Informationen aus diesem Angebot entstehen. Die Benutzenden stellen die Betreiber der Plattform 
«Rechercheportal Zeitungsauschnitte» von jeglichen Ansprüchen Dritter infolge Verletzung von Urheber- oder andere 
Immaterialgüterrechten frei.
</p></div></div></div>


</body>
</html>
""")

"""Hier wird mit Pyhton in HTML gearbeitet. Die Variable "zas-daten" wird in der for-Schleife aufgerufen.
Danach können die in der for-Schleife abgerufenen Werte in dieser Schleife gespeichert werden.
Diese werden im HTML beim Platzhalter %s eingefügt. """
zas_daten = """<html lang="en">
<body class="no-repeat"><p></p>

<!-- Platzhalter in HTML um Werte aus der for-Schlaufe einzufügen. -->
%s

</script></body>
</html>
"""

""""Die Metadaten in der IIIF-Datei werden nicht pro Zeitungsartikel, sondern pro pdf-Seite ausgegeben.
Damit Zeitungsartikel mit mehreren Seiten nicht mehrmals ausgegeben werden,
werden diese in der Liste "Dublette" gespeichert.
Hier wird diese Liste initiiert. """
dublette = []

"""Der Output für die csv-Datei wird in Listenform generiert,
da dieses Dateiformat die Werte auch durch Kommas trennt.
Da einige Werte bei einigen Zeitungsartikeln leer sind,
muss für die richtige Struktur des csv trotzdem ein Komma eingefügt werden.
Daher wird nach der Initialisierung der Liste bei allen möglichen Werten ein Komma eingefügt,
diese können dann, wenn Werte vorhanden sind, überschrieben werden."""
csv_liste = []
csv_liste.extend([ "",",", ",", ",",",",",",",", ",", ",", ",", ","])

"""Eine for-Schleife wird gestartet.
Diese wird solange durchgeführt, wie in der json-Datei Zeitungsartikel bzw. "items" beschrieben werden. """
for zas in range (len(json["items"])):
    """Damit bei mehrseitigen Artikel die Beschreibung nur einmal übernommen wird, 
    wird geprüft, ob der Artikel bereits in einem vorherigen Durchgang der Liste "Dublette" hinzugefügt wurde."""
    if json["items"][zas]["seeAlso"][0]["id"] not in dublette:
        """Die einzelnen Metadaten, hier z.B. der Link auf einen Artikel,
        werden durch "Slicing" abgefragt. Dass heisst, die json-Datei kann wie ein Dictionary abgefragt werden.
        Pro Durchgange der Schleife wird der nächste Eintrag durchgegangen. """
        link = json["items"][zas]["seeAlso"][0]["id"]      
        """Die Liste für den csv-Output wurde bei der Initialisierung mit Kommas für jeden Wert gefüllt.
        Falls aber ein Wert vorhanden ist, wird das Komma im csv-file an der richtigen Position gelöscht.
        Danach wird dan der gleichen Postion der Wert eingefügt."""
        del csv_liste[10]
        csv_liste.insert(10, link)
        
        dateiname = json["items"][zas]["seeAlso"][0]["label"]["de"][0]
        """Die abgefragten Metadaten werden hier mit Hilfe der Variable "zas_daten" vorbereitet um mit Hilfe
        des Platzhalters %s im HMTL-file zu erscheinen. Dieser Output wird wiederum in die Variable
        "html_output" gespeichert. Diese wird danach ins html_file geschrieben."""
        html_output = zas_daten  % "________________" +   "<p>" + "<b>PDF-Datei: </b> <a href='pdf/" + \
                      dateiname.replace("'",  "")+"'>" +dateiname+"</a></p>" 
        html_file.write(html_output)        
      
        #Hier wird die oben defnierte Funktion für den pdf-Download gestartet.
        download_file(json["items"][zas]["seeAlso"][0]["id"], json["items"][zas]["seeAlso"][0]["label"]["de"][0])

        """Alle Metadaten (ausser "Link" und "Dateiname") können mehrmals pro Artikel vorkommen.
        Eine weitere for-Schleife geht jeweils diese Metadaten durch."""        
        for metadaten in range (len(json["items"][zas]["metadata"])):
            """Die Metadaten stehen in der json-Datei nicht für jeden Artikel an der gleichen Stelle.
            Daher müssen zuerst die folgenden Kriterien geprüft werden:
            1. Handelt es sich um ein Metadatum, dass ich ausgeben will? Dies wird mit der Bezeichnung der
            Label geprüft.
            2. Gibt es beim gewünschten Metadatum überhaupt einen Inhalt?"""
            if json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Titel']: 
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    titel = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Titel: </b>" + liste_zu_string(titel) + "</p>"
                    html_file.write(html_output)
                    del csv_liste[0]
                    """Verschiedene Sonderzeichen können beim Öffnen von csv in Excel Probleme machen.
                    Da die Titel verschiedenste dieser Zeichen enthalten können, werden diese in Anführungszeichen gesetzt,
                    dies kann dann in Excel als String interpretiert werden."""
                    csv_liste.insert(0, '"' + liste_zu_string(titel) + '"')

            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Zeitungsname']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    zeitungsname = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Zeitungsname: </b>" + \
                                  liste_zu_string(zeitungsname).replace("\n", "")  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[1]
                    """Beim Zeitungsnamen gibt es fälschlicherweise ab und zu den Eintrag \n,
                    dieser muss ersetzt werden, da ansonsten das csv an dieser Stelle eine neue Zeile schreibt."""
                    csv_liste.insert(1, liste_zu_string(zeitungsname).replace("\n", ""))
            
            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Quelldatum']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    quelldatum = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Quelldatum: </b>" + \
                                  datumformatieren(liste_zu_string(quelldatum))  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[2]
                    csv_liste.insert(2, liste_zu_string(quelldatum))

            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Ausgabe']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    ausgabe = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Ausgabe: </b>" + liste_zu_string(ausgabe)  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[3]
                    csv_liste.insert(3, liste_zu_string(ausgabe))

            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Seite']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    seite = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Seite(n): </b>" + liste_zu_string(seite)  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[4]
                    """Semikolon machen beim Öffnen von csv in Excel Probleme. Daher werden diese ersetzt.
                    Sie kommen auch beim Metdatum "AutorIn" vor."""
                    csv_liste.insert(4, liste_zu_string(seite).replace(";", "|"))
                   
            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['AutorIn']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    autor = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Autor*innen: </b>" + liste_zu_string(autor)  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[5]
                    csv_liste.insert(5, liste_zu_string(autor).replace(";", "|"))
                   
            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Themen']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    themen = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Themen: </b>" + liste_zu_string(themen)  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[6]
                    csv_liste.insert(6, liste_zu_string(themen))
                   
            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Geografikum']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    geografikum = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Geografische Begriffe: </b>" + \
                                  liste_zu_string(geografikum)  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[7]
                    csv_liste.insert(7, liste_zu_string(geografikum))
                                
            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Personen']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    personen = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Personen: </b>" + liste_zu_string(personen)  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[8]
                    csv_liste.insert(8, liste_zu_string(personen))
                                                                    
            elif json["items"][zas]["metadata"][metadaten]["label"]["de"] == ['Firmen und Organisationen']:
                if len(json["items"][zas]["metadata"][metadaten]["value"]["de"]) >= 1:
                    firmen = json["items"][zas]["metadata"][metadaten]["value"]["de"]
                    html_output = zas_daten %  "<p>" + "<b>Firmen und Organisationen: </b>" + \
                                  liste_zu_string(firmen)  + "</p>"
                    html_file.write(html_output)
                    del csv_liste[9]
                    csv_liste.insert(9, liste_zu_string(firmen))
    
        """Bei jedem Durchgang wird die entstandene Liste mit den Metadaten in die nächste Zeile des 
        csv-files geschrieben.
        Danach wird der Inhalt der Liste gelöscht und danach wieder mit Kommas gefüllt.
        Damit beim nächsten Durchgang wieder die richtigen Metadaten überschrieben werden können."""
        csv_objekt.writerow(csv_liste)
        del csv_liste[:]
        csv_liste.extend([ "",",", ",", ",",",",",",",", ",", ",", ",", ","])
    """Am Schluss von jedem Durchgang der Schleife wird der aktuell ausgewertete Zeitungartikel 
    der Dubletten-Liste zugefügt.
    Damit dieser beim nächsten Durchgang nicht nochmals abgefragt wird."""                
    dublette.append(json["items"][zas]["seeAlso"][0]["id"]) 


#Die beiden HTML- und CSV-Dateien werden nicht mehr verwendet und daher geschlossen.
html_file.close() 
csv_file.close()


#Am Schluss wird der zip-Ordner geschlossen.
zip_ordner.write('style')
zip_ordner.write('index.html')
zip_ordner.write('metadaten.csv')
zip_ordner.write('favicon.ico', 'style\\favicon.ico')
zip_ordner.write('style_sheet.css', 'style\\style_sheet.css')
zip_ordner.write('swa_logo.svg', 'style\\swa_logo.svg')
zip_ordner.write('uni_logo.svg', 'style\\uni_logo.svg')
zip_ordner.close()

#Die erstellten Dateien werden entfernt, da diese bereits ins zip-Verzeichnis kopiert wurden.
os.remove("index.html")
os.remove("metadaten.csv")
